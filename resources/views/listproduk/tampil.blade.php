@extends('layout.master')

@section('judul')
Halaman Tampil Nama Produk
@endsection

@section('content')

<div class="row mb-3">
  <div class="col-md-4">
    <form action="/listproduk" method="GET">
      <div class="input-group">
        <input type="text" name="search" id="search" class="form-control" placeholder="Cari Nama Produk" value="{{ old('search') }}">
        <div class="input-group-append">
          <button class="btn btn-primary" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="fixed-buttons">
  @auth
  <a href="/listproduk/create" class="btn btn-primary my-3">Create Data</a>
  <a href="/listproduk" class="btn btn-success">Refresh</a>
  <a href="/exportlistproduk" class="btn btn-success">Export</a>

  <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Import
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Import Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/importlistproduk" method="post" enctype="multipart/form-data">
      <div class="modal-body">
          {{csrf_field()}}
          <div class="form-group">
           <input type="file" name="file" required="required">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Selesai</button>
          <button type="submit" class="btn btn-primary">Import Data</button>
        </div>
      </form>
    </div>
  </div>
</div>
  @endauth
  @include('sweetalert::alert')
</div>

<table class="table fixed-header">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Produk</th>
      <th scope="col">Action</th>
    </tr>
  </thead>

  <tbody>
    @forelse($listProduks as $key => $item)
    <tr>
      <th scope="row">{{$key + $listProduks->firstItem() }}</th>
      <td>{{$item->nama_produk}}</td>
      <td>
        <form action="/listproduk/{{$item->id}}" method="POST">
          @csrf
          @method('delete')
          @auth
          <a href="/listproduk/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
          <input type="Submit" onclick="return confirm('Apakah Kamu Yakin?')" value="Hapus" class="btn btn-sm btn-danger">
          @endauth
      </form>
      
    
      
      
      
      
      </td>
    </tr>
    @empty
    <h1>Tidak Ada Data Nama Produk</h1>
    @endforelse
  </tbody>
</table>
{{ $listProduks->links() }}
{{-- <div>
</div> --}}

@endsection

@section('styles')
<style>
  .fixed-buttons {
    position: fixed;
    top: 0;
    right: 0;
    margin-top: 50px;
    margin-right: 20px;
    z-index: 9999;
  }
</style>
@endsection



{{-- @section('scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  $(document).ready(function() {
    // Ketika user mengetik di input pencarian, lakukan filter data
    $('#search').on('keyup', function() {
      var keyword = $(this).val().toLowerCase();
      $('tbody tr').each(function() {
        if ($(this).text().toLowerCase().indexOf(keyword) == -1) {
          $(this).hide();
        } else {
          $(this).show();
        }
      });
    });
  });
</script>
@endsection --}}
