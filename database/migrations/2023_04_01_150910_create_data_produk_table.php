<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_produk', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->unsignedBigInteger('customor_id')->nullable();
            $table->foreign('customor_id')->references('id')->on('list_customor');
            $table->unsignedBigInteger('produk_id');
            $table->foreign('produk_id')->references('id')->on('list_produk');
            $table->unsignedBigInteger('berat_id');
            $table->foreign('berat_id')->references('id')->on('list_berat');
            $table->unsignedBigInteger('primer_id');
            $table->foreign('primer_id')->references('id')->on('list_primer');
            $table->unsignedBigInteger('sekunder_id');
            $table->foreign('sekunder_id')->references('id')->on('list_sekunder');
            $table->unsignedBigInteger('label_id');
            $table->foreign('label_id')->references('id')->on('list_label');
            $table->unsignedBigInteger('screen_id');
            $table->foreign('screen_id')->references('id')->on('list_screen');
            $table->unsignedBigInteger('exp_id');
            $table->foreign('exp_id')->references('id')->on('list_exp');
            $table->unsignedBigInteger('oven_id');
            $table->foreign('oven_id')->references('id')->on('list_oven');
            $table->unsignedBigInteger('rounding_id');
            $table->foreign('rounding_id')->references('id')->on('list_rounding');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_produk');
    }
}
