<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\List_customor;
class List_customorController extends Controller
{
   



    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
{
    $search = request()->input('search');
    $listcustomors = List_customor::when($search, function($query) use ($search) {
        return $query->where('nama_customor', 'like', "%$search%");
    })->get();
    return view('listcustomor.tampil', ['listcustomors'=>$listcustomors]);
}


    public function create()
    {
        return view('listcustomor.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_customor' => 'required',
        ]);

        List_customor::create([
            'nama_customor' => $request->nama_customor,
        ]);

        return redirect('/listcustomor');
    }

    public function edit($id)
    {
        $listcustomor = List_customor::where('id', $id)->first();
        return view('listcustomor.edit', ['listcustomor'=>$listcustomor]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_customor' => 'required',
        ]);

        $listcustomor = List_customor::find($id);

        $listcustomor->nama_customor = $request->nama_customor;
        $listcustomor->save();

        return redirect('/listcustomor');
    }

    public function destroy($id)
    {
        $listcustomor = List_customor::find($id);

        $listcustomor->delete();
        return redirect('/listcustomor');
    }
}


