<div class="sidebar" style="position: fixed; top: 0; left: 0; height: 100%; width: 250px; background-color: #343a40; overflow-y: auto;">
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="https://berkatcahayanovena.id/id/cc-content/themes/cicool/asset/bcn/img/icon/logo.svg" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="https://berkatcahayanovena.id" class="d-block">Alexander Pierce</a>
    </div>
  </div>
  <div class="form-inline">
    <div class="input-group" data-widget="sidebar-search">
      <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
        </button>
      </div>
    </div>
  </div>
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="text-align: left;">
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>Dashboard<i class="right fas fa-angle-left"></i></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="/listproduk" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>List Produk<i class="right fas fa-angle-left"></i></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="/listcustomor" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>List Customer<i class="right fas fa-angle-left"></i></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="/listberat" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>List Berat Produk<i class="right fas fa-angle-left"></i></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="../widgets.html" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>Widgets<span class="right badge badge-danger">New</span></p>
        </a>
      </li>
      @auth
      <li class="nav-item">
        <a href="/profile" class="nav-link">
          <i class="nav-icon fas fa-user"></i>
          <p>Profile</p>
        </a>
      </li>
      @endauth  
    </ul>
  </nav>
</div>