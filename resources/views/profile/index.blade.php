@extends('layout.master')

@section('judul')
Update Profile
@endsection

@section('content')
<form action="/profile/{{$detailProfile->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="inputUmur">Nama</label>
        <input type="text"  value="{{$detailProfile->user->name}}" class="form-control"   disabled>
        @error('Umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label for="inputUmur">Email</label>
        <input type="text"  value="{{$detailProfile->user->email}}" class="form-control"  disabled>
        @error('Umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="inputUmur">Departemen</label>
        <input type="text"  value="{{$detailProfile->departemen}}" class="form-control"  disabled>
        @error('departemen')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="inputAlamat">Jabatan</label>
        <input type="text"  value="{{$detailProfile->jabatan}}" class="form-control"  disabled>
        @error('jabatan')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <
    
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    
</form>

<style>
    textarea:focus {
        outline: none;
    }
    textarea::after {
        content: "";
        position: absolute;
        z-index: -1;
        top: 8px;
        bottom: 8px;
        left: 8px;
        right: 8px;
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.2);
        pointer-events: none;
    }
</style>

@endsection
