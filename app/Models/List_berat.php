<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class List_berat extends Model
{
    use HasFactory;
    protected $table ='list_berat';
    protected $fillable =['berat_produk'];
}
