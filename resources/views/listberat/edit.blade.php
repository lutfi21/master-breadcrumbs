@extends('layout.master')

@section('judul')
Halaman Edit Berat Produk
@endsection

@section('content')
<form action="/listberat/{{$listberat->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group row">
        <label for="inputNama" class="col-sm-2 col-form-label">Nama Customor</label>
        <div class="col-sm-4">
            <input type="text" value="{{$listberat->berat_produk}}" name="berat_produk" class="form-control" id="inputNama">
        </div>
    </div>
    @error('berat_produk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group row">
        <div class="col-sm-2"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection
