<?php

namespace App\Imports;

use App\Models\List_produk;
use Maatwebsite\Excel\Concerns\ToModel;

class ListProdukImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new List_produk([
            'nama_produk' => $row[1]
        ]);
    }
}
