<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\List_produk;

class Data_produkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function create()
    {
      $list_produk = List_produk::get();
      return view('dataproduk.tambah', ['list_produk' => $list_produk]);
    
    
        $list_customor = List_customor::get();
        return view('dataproduk.tambah', ['list_customor' => $list_customor]);
      }

    public function store(Request $request)
    {
 
     
       $request->validate([
         'kode' => 'required',
         'customor_id' => 'required',
         'produk_id'  => 'required|numeric',
         

       ]);


       $list_produk = New Film;
       $list_produk->kode = $request->kode;
       $list_produk->customor_id = $request->customor_i;
       $list_produk->produk_id = $request->produk_id;
       $list_produk->save();

       return redirect('/dataproduk');
    }
    public function index()
   {
    
    $data_produk = Data_Produk::all();
    return view('dataproduk.tampil', ['data_produk'=>$data_produk]);

   }
   public function show($id)
   {
    $data_produk = data_produk::find($id);
    return view('dataproduk.detail', ['data_produk'=>$data_produk]);
   }

   public function edit($id)
    {
      $data_produk = Data_produk::find($id);
      $listcustomor = List_customor::get();
      $listproduk  = List_produk::get();
        return view('dataproduk.edit', ['data_produk'=>$data_produk, 'list_customor'=>$list_customor, ]);
    }
    public function update($id, Request $request)
   {
    $request->validate([
        'kode' => 'required',
        'customor_id' => 'required',
        'produk_id'  => 'required',
    ]);

    $data_produk = Data_produk::find($id);
 
    $data_produk->kode = $request['kode'];
    $data_produk->customor_id = $request['customor_id'];
    $data_produk->produk_id = $request['produk_id'];
    $film->save();
    return redirect('/dataproduk');
    
   }

   public function destroy($id)
   {
    $data_produk = Data_produk::find($id);
    
    $data_produk->delete();
    return redirect('/dataproduk');
   }
}
