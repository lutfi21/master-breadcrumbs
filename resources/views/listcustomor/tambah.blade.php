@extends('layout.master')
@section('judul')
Halaman Tambah Customor
@endsection
@section('content')

<form action="/listcustomor" method="POST">
    @csrf
    
        
    <div class="form-group row">
      <label for="inputNama" class="col-sm-2 col-form-label">Nama Customor</label>
      <div class="col-sm-4">
        <input type="text" name="nama_customor" class="form-control" id="inputNama">
      </div>
    </div>
    @error('nama_customor')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
    <div class="form-group row">
      <div class="col-sm-2"></div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
    
  </form>
  
@endsection