@extends('layout.master')

@section('judul')
Halaman Tampil Nama Customor
@endsection

@section('content')

<div class="row mb-3">
  <div class="col-md-4">
    <form action="/listcustomor" method="GET">
      <div class="input-group">
        <input type="text" name="search" id="search" class="form-control" placeholder="Cari Nama Customor" value="{{ old('search') }}">
        <div class="input-group-append">
          <button class="btn btn-primary" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
@auth
    
<a href="/listcustomor/create" class="btn btn-primary my-3">Create Data</a>
<a href="/listcustomor" class="btn btn-success">Refresh</a>

@endauth

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Customor</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse($listcustomors as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->nama_customor}}</td>
        <td>

            
            <form action="/listcustomor/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                @auth
                    
                <a href="/listcustomor/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                <input type="Submit" onclick="return confirm('Apakah Kamu Yakin?')" value="Hapus" class="btn btn-sm btn-danger">
                @endauth
            
            </form>
        </td>
        
      </tr>
      @empty
      <h1>Tidak Ada Data Nama Customor</h1>
          
      @endforelse
     
    </tbody>
  </table>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  $(document).ready(function() {
    // Ketika user mengetik di input pencarian, lakukan filter data
    $('#search').on('keyup', function() {
      var keyword = $(this).val().toLowerCase();
      $('tbody tr').each(function() {
        if ($(this).text().toLowerCase().indexOf(keyword) == -1) {
          $(this).hide();
        } else {
          $(this).show();
        }
      });
    });
  });
</script>
@endsection
