<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\List_berat;
class List_beratController extends Controller
{
   



    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
{
    $search = request()->input('search');
    $listberats = List_berat::when($search, function($query) use ($search) {
        return $query->where('berat_produk', 'like', "%$search%");
    })->get();
    return view('listberat.tampil', ['listberats'=>$listberats]);
}


    public function create()
    {
        return view('listberat.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'berat_produk' => 'required',
        ]);

        List_customor::create([
            'berat_produk' => $request->berat_produk,
        ]);

        return redirect('/listberat');
    }

    public function edit($id)
    {
        $listberat = List_berat::where('id', $id)->first();
        return view('listberat.edit', ['listberat'=>$listberat]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'berat_produk' => 'required',
        ]);

        $listberat = List_customor::find($id);

        $listberat->berat_produk = $request->berat_produk;
        $listberat->save();

        return redirect('/listberat');
    }

    public function destroy($id)
    {
        $listberat = List_berat::find($id);

        $listberat->delete();
        return redirect('/listberat');
    }
}


