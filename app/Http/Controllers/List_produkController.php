<?php

namespace App\Http\Controllers;

use App\Models\List_produk;
use Illuminate\Http\Request;
use App\Exports\ListProdukExport;
use App\Imports\ListProdukImport;
use Maatwebsite\Excel\Facades\Excel;

class List_ProdukController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
{
    $search = request()->input('search');
    $listProduks = List_produk::when($search, function($query) use ($search) {
        return $query->where('nama_produk', 'like', "%$search%");
    })->paginate(5);
    
    return view('listproduk.tampil', ['listProduks'=>$listProduks]);
}

public function listprodukexport()
{
    return Excel::download(new ListProdukExport, 'listproduk.xlsx');
}

public function listprodukimport(Request $request)
{
$file = $request->file('file');
$namefile = $file->getClientOriginalName();
$file-> move('DataProduk', $namefile);

Excel::import(new ListProdukImport, public_path('/DataProduk/' . $namefile));
return redirect('/listproduk');
}
    public function create()
    {
        return view('listproduk.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required',
        ]);

        List_produk::create([
            'nama_produk' => $request->nama_produk,
        ]);

        return redirect('/listproduk')->with('toast_success', 'Data Sukses Ditambahkan');
    }

    public function edit($id)
    {
        $listProduk = List_produk::where('id', $id)->first();
        return view('listproduk.edit', ['listProduk'=>$listProduk]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_produk' => 'required',
        ]);

        $listProduk = List_produk::find($id);

        $listProduk->nama_produk = $request->nama_produk;
        $listProduk->save();

        return redirect('/listproduk');
    }

    public function destroy($id)
    {
        $listProduk = List_produk::find($id);

        $listProduk->delete();
        return redirect('/listproduk')->with('toast_success', 'Data Berhasil Dihapus!');
    }
}
