<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class List_customor extends Model
{
    use HasFactory;
    protected $table ='list_customor';
    protected $fillable =['nama_customor'];
}
