<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data_produk extends Model
{
    use HasFactory;
    protected $table ='data_produk';
    protected $fillable =['kode', 'customor_id', 'produk_id'];

    public function list_customor(){
        return $this->belongsTo(List_customor::class, 'list_customor');
    }
    public function list_produk(){
        return $this->belongsTo(List_produk::class, 'list_produk');
    }
}
