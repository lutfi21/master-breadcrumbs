@extends('layout.master')
@section('judul')
Halaman Tambah Customor
@endsection
@section('content')

<form action="/listberat" method="POST">
    @csrf
    
        
    <div class="form-group row">
      <label for="inputNama" class="col-sm-2 col-form-label">Berat Produk</label>
      <div class="col-sm-4">
        <input type="text" name="berat_produk" class="form-control" id="inputNama">
      </div>
    </div>
    @error('berat_produk')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
    <div class="form-group row">
      <div class="col-sm-2"></div>
      <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
    
  </form>
  
@endsection