<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\List_produkController;
use App\Http\Controllers\List_customorController;
use App\Http\Controllers\List_beratController;
use App\Http\Controllers\Data_produkController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('profile', ProfileController::class)->only(['index','update']);

Route::get('/', [HomeController::class, 'index']);



Route::get('/welcome', [HomeController::class, 'welcome']);

Route::post('/kirim', [HomeController::class, 'kirim']);

//CRUD Katagori
//untuk aksi simpan data ke database

Route::post('/listproduk', [List_produkController::class, 'store']);
    
//Update Data
Route::get('listproduk/{id}/edit', [List_produkController::class, 'edit']);
//Update data di database berdasarkan id
Route::put('/listproduk/{id}', [List_produkController::class, 'update']);

//Delete Data
route::delete('/listproduk/{id}', [List_produkController::class, 'destroy']);

//create data
//menuju ke inputan tambah data
Route::get('/listproduk/create', [List_produkController::class, 'create']);
Route::get('/exportlistproduk', [List_produkController::class, 'listprodukexport'])->name('exportlistproduk');
Route::post('/importlistproduk', [List_produkController::class, 'listprodukimport'])->name('importlistproduk');


//Route Untuk Menampilkan Semua Data
Route::get('/listproduk', [List_produkController::class, 'index']);
//Route Dinamis yang bisa get data dari detail berdasarkan id

//route::get('/listproduk/{id}', [List_produkController::class, 'show']);

//CRUD Katagori
//untuk aksi simpan data ke database

Route::post('/listcustomor', [List_customorController::class, 'store']);
    
//Update Data
Route::get('listcustomor/{id}/edit', [List_customorController::class, 'edit']);
//Update data di database berdasarkan id
Route::put('/listcustomor/{id}', [List_customorController::class, 'update']);

//Delete Data
route::delete('/listcustomor/{id}', [List_customorController::class, 'destroy']);

//create data
//menuju ke inputan tambah data
Route::get('/listcustomor/create', [List_customorController::class, 'create']);


//Route Untuk Menampilkan Semua Data
Route::get('/listcustomor', [List_customorController::class, 'index']);
//Route Dinamis yang bisa get data dari detail berdasarkan id

//route::get('/listcustomor/{id}', [List_customorController::class, 'show']);

//Create Data Film
Route::post('/dataproduk', [Data_produkController::class, 'store']);
    
//Update Data
route::get('/dataproduk/{id}/edit', [Data_rodukController::class, 'edit']);
//Update data di database berdasarkan id
route::put('/dataproduk/{id}', [Data_rodukController::class, 'update']);

//Delete Data
route::delete('/dataproduk/{id}', [Data_produkController::class, 'destroy']);

//create data
//menuju ke inputan tambah data
Route::get('/dataproduk/tambah', [Data_produkController::class, 'create']);
 
//Route Untuk Menampilkan Semua Data
Route::get('/dataproduk', [Data_produkController::class, 'index']);
//Route Dinamis yang bisa get data dari detail berdasarkan id
route::get('/dataproduk/{id}', [Data_produkController::class, 'show']); 

//Create Data Film
Route::post('/listberat', [List_beratController::class, 'store']);
    
//Update Data
route::get('/listberat/{id}/edit', [List_beratController::class, 'edit']);
//Update data di database berdasarkan id
route::put('/listberat/{id}', [List_beratController::class, 'update']);

//Delete Data
route::delete('/listberat/{id}', [List_beratController::class, 'destroy']);

//create data
//menuju ke inputan tambah data
Route::get('/listberat/tambah', [List_beratController::class, 'create']);
 
//Route Untuk Menampilkan Semua Data
Route::get('/listberat', [List_beratController::class, 'index']);
//Route Dinamis yang bisa get data dari detail berdasarkan id
route::get('/listberat/{id}', [List_beratController::class, 'show']);