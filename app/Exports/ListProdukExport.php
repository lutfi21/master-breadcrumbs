<?php

namespace App\Exports;

use App\Models\List_produk;
use Maatwebsite\Excel\Concerns\FromCollection;

class ListProdukExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return List_produk::all();
    }
}
